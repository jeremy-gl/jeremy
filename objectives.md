# Objectives

### Near-term

Before I'm OOO in early July (1st, 3rd, 4th, and 8th), I'd like to get a few things done:

* [ ]  Respond to the [forks issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/15082)
* [x]  Summarize [planning for Access in 12.2](https://gitlab.com/gitlab-org/manage/issues/50)
* [x]  Follow up on the [SSO testing and rollout plan](https://gitlab.com/gitlab-org/manage/issues/41)
* [x]  Create all issues for [GitLab.com visibility restriction](https://gitlab.com/gitlab-org/gitlab-ee/issues/12388#note_186605442)
* [x]  Write an onboarding document for Jensen
* [ ]  Clear my inbox

### Longer-term

* [ ]  All strategy pages for Access
* [ ]  Roadmap for Access to EOY
* [ ]  All strategy pages for Adopt
* [ ]  Roadmap for Access for 3+ releases

(Merge this with my personal TODO doc)